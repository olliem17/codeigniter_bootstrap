<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="inner cover">
	<h1 class="cover-heading">News</h1>

<?php foreach ($news as $news_item): ?>

        <h3><?php echo $news_item['title']; ?></h3>
        <div class="main">
                <?php echo $news_item['text']; ?>
        </div>
        <p><a href="<?php echo site_url('news/'.$news_item['slug']); ?>">View article</a></p>

<?php endforeach; ?>

</div>
